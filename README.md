# iml2gradle

This tool is used to generate gradle build scripts for projects defined using iml files.

Usage: iml2gradle module1 module2 module3 ...
	This tool is meant to be used at the source package root.
OPTIONS:

-h --help -> prints out help.

--origin=path -> Use this to set the project root location if you aren't calling the tool from the project root.
